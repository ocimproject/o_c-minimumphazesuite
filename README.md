# A minimalist clone of the Phazerville Ornament and Crime firmware project
===

### This is based off the Dec 19th 2023 [**v1.6.999**](https://github.com/djphazer/O_C-BenisphereSuite/releases/tag/PSv1.6.999) release 

This project is primarly used as the base for the Ornament and Crime Image Manager (OCIM) [https://gitlab.com/ocimproject/ocim]

The [**Phazerville**](https://github.com/djphazer/O_C-BenisphereSuite) version of Benisphere/Hemisphere is great and tries to make it easy by adding some default apps an a collection of premade configs for different useful images.

There are those eurorack enthusiasts however who are looking to get fully custom and build a truely personal image with no default apps.  Thus this project was born.  I have made no structural changes to the Phazerville version other than making his default apps optional.

*This does assume you have a reasonable knowledge of using the other O_C firmware projects. It is not massively user friendly*

Note that not all combinations of apps will work and in particular the O_C only has so much memory so if you get a build size error from ```pio run``` then remove some.

===


### How do I build it?

This works with the PlatformIO build process in the same way as the Phazerville version.

The simplified version being:
 - Install the PlatformIO cli (or get IDE plugin): https://platformio.org/
 - Modify the platformio.ini file in software/o_c_REV/
 - Then from o_c_REV/ 
 ```
 pio run
 ```
 -  Then upload .hex file in software/o_c_REV/.pio/build/minimum (unless you changed the en:minimum to something else) via Teensy Loader or however you would Phzerville/Benisphere

## Credits

### I am leaving this credits from the Phazerville project here as they are still relevant. But will add thank you to [**DJ Phazer**](https://www.phazerville.com/) for creating his addition to the collection of great O_C projects.

Many minds before me have made this project possible. Attribution is present in the git commit log and within individual files.
Shoutouts:
* **[Logarhythm1](https://github.com/Logarhythm1)** for the incredible **TB-3PO** sequencer, as well as **Stairs**.
* **[herrkami](https://github.com/herrkami)** and **Ben Rosenbach** for their work on **BugCrack**.
* **[benirose](https://github.com/benirose)** also gets massive props for **DrumMap** and the **ProbDiv / ProbMelo** applets.
* **[qiemem](https://github.com/qiemem)** (Bryan Head) for the **Ebb&LFO** applet and its _tideslite_ backend, among other things.

And, of course, thank you to **[Chysn](https://github.com/Chysn)** for the clever applet framework from which we've all drawn inspiration.

This is a fork of [Benisphere Suite](https://github.com/benirose/O_C-BenisphereSuite) which is a fork of [Hemisphere Suite](https://github.com/Chysn/O_C-HemisphereSuite) by Jason Justian (aka chysn).

ornament**s** & crime**s** is a collaborative project by Patrick Dowling (aka pld), mxmxmx and Tim Churches (aka bennelong.bicyclist) (though mostly by pld and bennelong.bicyclist). it **(considerably) extends** the original firmware for the o_C / ASR eurorack module, designed by mxmxmx.

http://ornament-and-cri.me/
